# Jobs API

## Task Details
Backend Test Project - Job Board
Your task is to implement a simple Job Board backend API. Detailed specifications for the test project are provided below. We estimate that you will not need more than a single weekend at relaxed coding speed to implement it.

Project Description
The Job Board API will be used by your Users (Job Seekers) to perform the following tasks:

List Job Posts
Apply to Jobs
Every User (Job Seeker) will have their own job applications that have statuses that can be checked(seen, not seen). Job Posts are managed by other Users (Admin).

The Job Board app will be used by your Admin User to perform the following tasks:

Manage A Job Post
List Job Applications
List Job Posts

## Development Environment Setup

### Tools
* Ruby On Rails [Tutorial](https://gorails.com/setup/osx/11-big-sur).
* Git [Tutorial](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
* IDE (VS Code) [Tutorial](https://code.visualstudio.com/docs/setup/mac).

### Run App
* Clone: `git clone https://aisha_alkhateeb@bitbucket.org/aisha_alkhateeb/jobs-api.git`.
* Install dependency: `bundle install`.
* Run `rake db:migrate`.
* Run `rake db:seed`.  <!-- fill the database with its default values. -->
* Start App `rails server`.

## Things to note
If you faced errors with 'twitter-bootstrap-static/fontawesome'
	- Make sure that you have it setUp correctly in your environment
		check the following link for more details: https://github.com/seyhunak/twitter-bootstrap-rails/blob/master/README.md
