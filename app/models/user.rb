class User < ApplicationRecord
  after_initialize :set_default_role

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: [:user, :company, :admin]

  has_many :jobs, :foreign_key => 'company_id', :dependent => :destroy
  has_many :job_applications, :foreign_key => 'user_id'

  def set_default_role
    self.role ||= :user
  end
end
