# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


company = User.create!({:email => "company@email.com", :password => "password", :password_confirmation => "password", :role => 1 })
company = User.create!({:email => "user@email.com", :password => "password", :password_confirmation => "password", :role => 0 })

Category.create!(name: "Full-Time")
Category.create!(name: "Part-Time")
Category.create!(name: "Remote")
Category.create!(name: "Internship")


Job.create!(
 title: "Oracle Forms and Reports Developer",
 description: "Catania Solutions looking for an Oracle Forms and Reports Developer with good analyst skills to support multiple reporting projects. The candidate is a resource that can work with business team to get requirements, design and develop complex data transformations to support reporting work from reporting tool.",
 company_name: "Rimal Capital",
 url: "https://www.akhtaboot.com/en/companies/458557-Rimal-Capital",
 location: "Amman",
 category_id: 1,
 company_id: company.id
)

Job.create!(
 title: "Associate Java Developer",
 description: "We are looking to recruit outstanding fresh Java Developers who will receive an intensive training in the field of Java Development after which they would be involved in designing and implementing server-side components of the software products as part of a team developing a financial service using cutting-edge technologies.",
 company_name: "ProgressSoft",
 url: "https://www.akhtaboot.com/en/companies/216573-ProgressSoft",
 location: "Amman",
 category_id: 1,
 company_id: company.id
)

Job.create!(
 title: "Process Engineer",
 description: "ELREHA (Elektronische Regelungen GmbH) was established in 1976. We are an international leader in development, production and sales of electronic controllers and control systems, for industrial and commercial needs in heating, refrigeration and freezing applications",
 company_name: "ELREHA JORDAN",
 url: "https://www.akhtaboot.com/en/jordan/jobs/zarqa/102009-Process-Engineer-at-ELREHA-JORDAN",
 location: "Zarqa",
 category_id: 2,
 company_id: company.id
)

Job.create!(
 title: "Shelter Team Leader - Design Internals & Nationals Only",
 description: "The Shelter Team Leader is responsible for leading a team of architects producing technical designs for school construction / expansion projects",
 company_name: "Norwegian Refugee Council (NRC)",
 url: "https://www.akhtaboot.com/en/companies/62383--Norwegian-Refugee-Council--NRC-",
 location: "Irbid",
 category_id: 3,
 company_id: company.id
)

Job.create!(
 title: "Senior Software Engineer",
 description: "We are looking for smart, talented well-rounded developers to join our team. If you are motivated, detail-oriented and looking for a challenge, we would love to hear from you.",
 company_name: "ZenHR Solutions",
 url: "https://www.akhtaboot.com/en/jordan/jobs/amman/102309-Senior-Software-Engineer--at-ZenHR-Solutions",
 location: "Amman",
 category_id: 1,
 company_id: company.id
)
